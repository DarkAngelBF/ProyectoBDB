package facci.com.proyectobdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HelperBD extends SQLiteOpenHelper {
    //constructores
    public HelperBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    //metodos
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE PERSONA(id INTEGER PRIMARY KEY AUTOINCREMENT, cedula TEXT, " +
                "apellidos TEXT, nombres TEXT);");
        //que ejecutaría su declaración de creación de tabla en la base de datos SQLite de su aplicación
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    public void insertar (String cedula, String apellidos, String nombres){
        ContentValues values = new ContentValues();
        values.put("cedula", cedula);
        values.put("apellidos", apellidos);
        values.put("nombres", nombres);
        this.getWritableDatabase().insert("persona", null, values);
    }

    public void modificar(String cedula, String apellidos, String nombres){
        ContentValues values = new ContentValues();
        values.put("apellidos" , apellidos);
        values.put("nombres", nombres);
        this.getWritableDatabase().update("persona", values,"cedula='" + cedula + "'",null);
    }

    //sirve como una compuerta de escape donde puedes construir tu propia
    // consulta SQL en tiempo de ejecución pero aún usar Room para convertirla en objetos.
    public String leerPorCedula(String cedula){
        String consulta = "";
        String [] args = new String[]{cedula};
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM persona WHERE cedula=?", args);
        if(cursor.moveToFirst()){
            do{
                String cedulaPersona = cursor.getString(cursor.getColumnIndex("cedula"));
                String apellidosPersona = cursor.getString(cursor.getColumnIndex("apellidos"));
                String nombresPersona = cursor.getString(cursor.getColumnIndex("nombres"));
                consulta += cedulaPersona + " " + apellidosPersona + " " + nombresPersona + "\n";
            }while(cursor.moveToNext());
        }
        return consulta;
    }

    public String leerTodos(){
        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM persona", null);
        if(cursor.moveToFirst()){
            do{
                String cedulaPersona = cursor.getString(cursor.getColumnIndex("cedula"));
                String apellidosPersona = cursor.getString(cursor.getColumnIndex("apellidos"));
                String nombresPersona = cursor.getString(cursor.getColumnIndex("nombres"));
                consulta += cedulaPersona + " " + apellidosPersona + " " + nombresPersona + "\n";
            }while(cursor.moveToNext());
        }
        return consulta;
    }

    public void eliminarTodos(){
        this.getWritableDatabase().delete("persona", null, null);
    }

    public void eliminarPorCedula(String cedula){
        this.getWritableDatabase().delete("persona","cedula='"+cedula + "'", null);
    }


}





























