package facci.com.proyectobdb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText cajaCedula, cajaApellidos, cajaNombres;
    Button botonAgregar, botonModificar, botonEliminar, botonListar, botonEliminarTodos, botonListarTodos;
    TextView datos;
    HelperBD personabd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cajaCedula = (EditText)findViewById(R.id.txtCedula);
        cajaApellidos = (EditText)findViewById(R.id.txtApellidos);
        cajaNombres = (EditText)findViewById(R.id.txtNombres);
        botonAgregar = (Button)findViewById(R.id.btnAgregar);
        botonModificar = (Button)findViewById(R.id.btnModificar);
        botonEliminar = (Button)findViewById(R.id.btnEliminar);
        botonListar = (Button)findViewById(R.id.btnListar);
        botonEliminarTodos = (Button)findViewById(R.id.btnEliminarTodos);
        botonListarTodos = (Button)findViewById(R.id.btnListarTodos);
        datos = (TextView)findViewById(R.id.lblDatos);

        //llamar los eventos de los botones
        botonAgregar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonEliminarTodos.setOnClickListener(this);
        botonListarTodos.setOnClickListener(this);

        personabd = new HelperBD(this,"bda3", null, 1);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgregar:
                if (cajaCedula.getText().toString().isEmpty()){

                    Toast.makeText(getApplicationContext(),"Debe llenar campo cedula",
                            Toast.LENGTH_LONG).show();                }

                            else {
                personabd.insertar(cajaCedula.getText().toString(), cajaApellidos.getText().toString(),
                        cajaNombres.getText().toString());
                    Toast.makeText(getApplicationContext(),"Agregado con éxito",
                            Toast.LENGTH_LONG).show();

                    cajaNombres.setText("");
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    datos.setText("");

                }

            break;
            case R.id.btnModificar:
                if (cajaCedula.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(),"Debe llenar campo cedula, del registro a modificar",
                        Toast.LENGTH_LONG).show();} else {
                personabd.modificar(cajaCedula.getText().toString(), cajaApellidos.getText().toString()
                                    , cajaNombres.getText().toString());
                    Toast.makeText(getApplicationContext(),"Modificado con éxito",Toast.LENGTH_LONG).show();
                    cajaNombres.setText("");
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    datos.setText("");


                }
                break;
            case R.id.btnEliminar:
                if (cajaCedula.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Debe llenar campo cedula, del registro a eliminar",
                        Toast.LENGTH_LONG).show();} else{
            personabd.eliminarPorCedula(cajaCedula.getText().toString());
                    Toast.makeText(getApplicationContext(),"Eliminado con éxito",Toast.LENGTH_LONG).show();
                    cajaNombres.setText("");
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    datos.setText("");

                }
                break;
            case R.id.btnListar:
                if(cajaCedula.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Listado",Toast.LENGTH_LONG).show();
                    cajaNombres.setText("");
                    cajaApellidos.setText("");
                    cajaCedula.setText("");
                    datos.setText("");
                } else {
                datos.setText(personabd.leerPorCedula(cajaCedula.getText().toString()));}
                break;
            case R.id.btnEliminarTodos:


                personabd.eliminarTodos();
                Toast.makeText(getApplicationContext(),"Todos los registros eliminados",
                        Toast.LENGTH_LONG).show();
                cajaNombres.setText("");
                cajaApellidos.setText("");
                cajaCedula.setText("");
                datos.setText("");
                break;
            case R.id.btnListarTodos:
                datos.setText(personabd.leerTodos());
                Toast.makeText(getApplicationContext(),"Todos los registros Mostrados",
                        Toast.LENGTH_LONG).show();
                cajaNombres.setText("");
                cajaApellidos.setText("");
                cajaCedula.setText("");
                break;
        }
    }
}
